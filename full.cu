#include <iostream>
#include <cstdlib>
#include <cuda.h>
#include <time.h>

//API Debugera
using namespace std;

long double alloc_size=0;
bool debug = false;

void print_device(double * mat, unsigned int h, unsigned int w){
  double * temp = new double [h*w];
  cudaMemcpy(temp,mat,sizeof(double)*h*w,cudaMemcpyDeviceToHost);
  if(debug==true)cout<<"014-Kopiowanie pamieci - print "<<mat<<" "<<sizeof(double)*h*w<<"\n";
  for(int i=0;i<h;i++){
    for(int j=0;j<w;j++){
      cout<<temp[i*w+j]<<" ";  
    }  
    cout<<endl;
  }
  cout<<endl;
  delete temp;
}
//KONIEC

//Warstwa sieci

class Neuron_layer{
  private:
  //public:
  double * device_Scales_mat;

  double * device_output_vector;

  unsigned int input_size;
  unsigned int output_size;
  double learning_ratio;
  public:
  double * get_output_vector(){return device_output_vector;}
  double * get_Scales_mat(){return device_Scales_mat;}
  void calculate_layer(double* X_Vector);
  void Learn(double * X_Vector, double * delta_error);
  Neuron_layer(){}
  void set_default(unsigned int in, unsigned int out,double ratio){
    cudaMalloc((void**)&device_Scales_mat,sizeof(double)*in*out);
    if(debug==true)cout<<"041-Zalokowano: "<<sizeof(double)*in*out<<" pod adresem "<<device_Scales_mat<<" dla warstwy: "<<this<<endl;
    alloc_size+=sizeof(double)*in*out;
    cudaMalloc((void**)&device_output_vector,sizeof(double)*out);
    if(debug==true)cout<<"044-Zalokowano: "<<sizeof(double)*out<<" pod adresem "<<device_output_vector<<" dla warstwy: "<<this<<endl;
    alloc_size+=sizeof(double)*out;

    learning_ratio = ratio;
    input_size = in;
    output_size = out;
    
    double rand_num;
    //if(debug==true)
      cout<<"Randomizacja wag "<<device_Scales_mat<<" do "<<device_Scales_mat+in*out-1<<endl;
    for(unsigned int i=0; i<in*out; i++){
      rand_num = 1;
      //rand()%10000*0.0001;
      //if(debug==true)cout<<rand_num<<endl;
      cudaMemcpy(this->device_Scales_mat+i, &rand_num, sizeof(double), cudaMemcpyHostToDevice);
      //if(debug==true)cout<<"62 - Kopiowanie pamieci "<<device_Scales_mat+i<<" "<<&rand_num<<" "<<sizeof(double)<<" H-T-D"<<endl;
    }
    cout<<"stop\t"<<this<<endl;
  }
  void set_ratio(double ratio){learning_ratio = ratio;}

  Neuron_layer(unsigned int in, unsigned int out,double ratio){
    cudaMalloc((void**)&device_Scales_mat,sizeof(double)*in*out);
    if(debug==true)cout<<"062-Zalokowano: "<<sizeof(double)*in*out<<" pod adresem "<<device_Scales_mat<<" dla warstwy: "<<this<<endl;
    alloc_size+=sizeof(double)*in*out;
    cudaMalloc((void**)&device_output_vector,sizeof(double)*out);
    if(debug==true)cout<<"064-Zalokowano: "<<sizeof(double)*out<<" pod adresem "<<device_output_vector<<" dla warstwy: "<<this<<endl;
    alloc_size+=sizeof(double)*out;

    learning_ratio = ratio;
    input_size = in;
    output_size = out;
    
    double rand_num;
    //if(debug==true)
      cout<<"Randomizacja wag "<<device_Scales_mat<<" do "<<device_Scales_mat+in*out-1<<endl;
    for(unsigned int i=0; i<in*out; i++){
      rand_num = 1;
      //rand()%10000*0.0001;
      //if(debug==true)cout<<rand_num<<endl;
      cudaMemcpy(this->device_Scales_mat+i, &rand_num, sizeof(double), cudaMemcpyHostToDevice);
      //if(debug==true)cout<<"85 - Copiowanie pamieci "<<device_Scales_mat+i<<" "<<&rand_num<<" "<<sizeof(double)<<" H-T-D"<<endl;
    }
    cout<<"stop\t"<<this<<endl;
  }
  ~Neuron_layer(){
    cudaFree(device_Scales_mat);
    if(debug==true)cout<<"081-Free: "<<device_Scales_mat<<endl;
    cudaFree(device_output_vector);
    if(debug==true)cout<<"083-Free: "<<device_output_vector<<endl;
  }
};
__global__ void pre_sum(double * Scales, double* X_Vector, double* out){
  unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
  extern __shared__ double temp[];
  temp[threadIdx.x] = X_Vector[threadIdx.x];
  __syncthreads();
  out[i]=Scales[i]*temp[threadIdx.x];
  //out[i]=Scales[i]*X_Vector[i];
}

__global__ void sum_row(double* mat,double* out,unsigned int size){
  double sum=0;
  for(unsigned int i=0;i<size;i++){
    sum+=mat[threadIdx.x*size+i];
  }  
  out[threadIdx.x]=sum;
}

__global__ void activation_foo(double * sum_vector, double * output_vector){
  output_vector[threadIdx.x]=__drcp_rn(1+ exp(-sum_vector[threadIdx.x]));
}

__global__ void derivative(double * input, double * output ){  
  output[threadIdx.x]=__dmul_rn(input[threadIdx.x],(1-input[threadIdx.x]));
}

__global__ void mul_matrix(double * mat, double pam,double * ret){
   unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
   ret[i]=(mat[i]*pam);
}

__global__ void mul_matrix_cross(double * A , double * B,double * C){
    unsigned int i= blockDim.x*blockIdx.x + threadIdx.x;
    extern __shared__ double temp [];
    temp[threadIdx.x] = B[threadIdx.x];
    __syncthreads();
    C[i]=__dmul_rn(A[blockIdx.x],temp[threadIdx.x]);
}

__global__ void matrix_update(double * A , double * B, double * C){
  unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
  C[i]=A[i]+B[i];
}

void Neuron_layer::calculate_layer(double* X_Vector_device){
  
  double * d_p_sum;
  cudaMalloc((void**)&d_p_sum,sizeof(double)*input_size*output_size);
  if(debug==true)cout<<"129-Zalokowano: "<<sizeof(double)*input_size*output_size<<" pod adresem "<<d_p_sum<<" dla warstwy: "<<this<<endl;
  alloc_size+=sizeof(double)*input_size*output_size;

  pre_sum<<<this->output_size, this->input_size, this->input_size*sizeof(double)>>>(this->device_Scales_mat,X_Vector_device, d_p_sum);
  
  cudaDeviceSynchronize();
  double * d_sum_vector;
  cudaMalloc((void**)&d_sum_vector,sizeof(double)*this->output_size);
  if(debug==true)cout<<"136-Zalokowano: "<<sizeof(double)*output_size<<" pod adresem "<<d_sum_vector<<" dla warstwy: "<<this<<endl;
  alloc_size+=sizeof(double)*output_size;

  sum_row<<<1, this->output_size>>>(d_p_sum,  d_sum_vector,  this->input_size);

  cudaFree(d_p_sum);
  if(debug==true)cout<<"144-Free: "<<d_p_sum<<endl;
  cudaDeviceSynchronize();

  activation_foo<<<1, this->output_size>>>(d_sum_vector,this->device_output_vector);
  /*double * dada = new double [output_size];
  for(int i=0;i<output_size;i++){
    dada[i]=i;
  }
  cudaMemcpy(device_output_vector,dada,sizeof(double)*output_size,cudaMemcpyHostToDevice);
  if(debug==true)cout<<"164-Kopiowanie pamieci: "<<device_output_vector<<" "<<dada<<" "<<sizeof(double)*output_size<<" H-T-D"<<endl;
  */cudaFree(d_sum_vector);
  if(debug==true)cout<<"154-Free: "<<d_sum_vector<<endl;
  cudaDeviceSynchronize();
  return;
}
void Neuron_layer::Learn(double * X_Vector, double * delta_error){
  //this->print_scales();
  
  this->calculate_layer(X_Vector);
  
  double * de;
  cudaMalloc((void**)&de,sizeof(double)*output_size);
  if(debug==true)cout<<"160-Zalokowano: "<<sizeof(double)*output_size<<" pod adresem "<<de<<" dla warstwy: "<<this<<endl;
  alloc_size+=sizeof(double)*output_size;
  
  derivative<<<1,output_size>>>(this->get_output_vector(),de);
  //print_device(de,output_size,1);
  
  double * iii;
  cudaMalloc((void**)&iii,sizeof(double)*output_size);
  if(debug==true)cout<<"167-Zalokowano: "<<sizeof(double)*output_size<<" pod adresem "<<iii<<" dla warstwy: "<<this<<endl;
  alloc_size+=output_size*sizeof(double);

  pre_sum<<<1,output_size,output_size>>>(de,delta_error,iii);
  //print_device(iii,output_size,1);

  double * param;
  cudaMalloc((void**)&param,sizeof(double)*output_size);
  if(debug==true)cout<<"174-Zalokowano: "<<sizeof(double)*output_size<<" pod adresem "<<param<<" dla warstwy: "<<this<<endl;
  alloc_size+=sizeof(double)*output_size;

  mul_matrix<<<1,output_size>>>(iii,learning_ratio,param);
  //print_device(param,output_size,1);

  //print_device(X_Vector,1,input_size);

  double * delta_scales;
  cudaMalloc((void**)&delta_scales,sizeof(double)*input_size*output_size);
  if(debug==true)cout<<"183-Zalokowano: "<<sizeof(double)*output_size*input_size<<" pod adresem "<<delta_scales<<" dla warstwy: "<<this<<endl;
  alloc_size+=sizeof(double)*output_size*input_size;

  mul_matrix_cross<<<output_size,input_size,input_size*sizeof(double)>>>(param,X_Vector,delta_scales);
  //print_device(delta_scales,output_size,input_size);

  double * temp;
  cudaMalloc((void**)&temp,sizeof(double)*input_size*output_size);
  if(debug==true)cout<<"190-Zalokowano: "<<sizeof(double)*input_size*output_size<<" pod adresem "<<temp<<" dla warstwy: "<<this<<endl;
  alloc_size+=sizeof(double)*input_size*output_size;

  matrix_update<<<output_size,input_size>>>(this->device_Scales_mat,delta_scales,temp);
  //print_device(temp,output_size,input_size);
  
  cudaFree(device_Scales_mat);
  if(debug==true)cout<<"201-Free: "<<device_Scales_mat<<endl;
  device_Scales_mat=temp;

  cudaFree(delta_scales);
  if(debug==true)cout<<"205-Free: "<<delta_scales<<endl;
  cudaFree(param);
  if(debug==true)cout<<"207-Free: "<<param<<endl;
  cudaFree(iii);
  if(debug==true)cout<<"209-Free: "<<iii<<endl;
  cudaFree(de);
  if(debug==true)cout<<"211-Free: "<<de<<endl;
  //this->print_scales();
  return;
}

//Koniec warstwy sieci

//ANN Struct - struktura calej sieci
class ANN_Struct{
public:
void cuda_cal	(double * device_input_vector);
void cal	(double * input_vector){
  double * temp;
  cudaMalloc((void**)&temp,sizeof(double)*input_size);
  if(debug==true)cout<<"215-Zalokowano: "<<sizeof(double)*input_size<<" pod adresem "<<temp<<" dla sieci: "<<this<<endl;
  alloc_size+=sizeof(double)*input_size;

  cudaMemcpy(temp,input_vector,sizeof(double)*input_size,cudaMemcpyHostToDevice);
  if(debug==true)cout<<"246-Kopiowanie pamieci "<<temp<<" "<<input_vector<<" "<<sizeof(double)*input_size<<" H-T-D"<<endl;
  cuda_cal(temp);
  cudaFree(temp);
  if(debug==true)cout<<"230-Free: "<<temp<<endl;
}
void cudaLearn	(double * device_input_vector, double * device_output_vector);
void Learn	(double * input_vector, double * output_vector);

double * get_device_output_vector(){return Output_output;}
private:
Neuron_layer * Input_neuron_layer;
Neuron_layer * Hiden_1_layer;
Neuron_layer * Hiden_2_layer;
Neuron_layer * Hiden_3_layer;
Neuron_layer * Output_neuron_layer;

double * Input_output;
double * Hiden_1_output;
double * Hiden_2_output;
double * Hiden_3_output;
double * Output_output;

unsigned int input_size;
unsigned int hiden_size;
unsigned int output_size;
public:
ANN_Struct(unsigned int input,unsigned int hiden_size ,unsigned int output, double learning_ratio);
~ANN_Struct();

};

ANN_Struct::ANN_Struct(unsigned int input,unsigned int hiden ,unsigned int output, double learning_ratio){
  input_size	      =	  input;
  hiden_size	      =	  hiden;
  output_size	      =	  output;

  Input_neuron_layer  =	  new Neuron_layer(input,hiden_size,learning_ratio);
  Hiden_1_layer	      =	  new Neuron_layer(hiden_size,hiden_size,learning_ratio);
  Hiden_2_layer	      =	  new Neuron_layer(hiden_size,hiden_size,learning_ratio);
  Hiden_3_layer	      =	  new Neuron_layer(hiden_size,hiden_size,learning_ratio);
  Output_neuron_layer =	  new Neuron_layer(hiden_size,output,learning_ratio);
}
ANN_Struct::~ANN_Struct(){
  delete Input_neuron_layer;
  delete Hiden_1_layer;
  delete Hiden_2_layer;
  delete Hiden_3_layer;
  delete Output_neuron_layer;

  cudaFree(Input_output);
  if(debug==true)cout<<"271-Free "<<Input_output<<endl;
  cudaFree(Hiden_1_output);
  if(debug==true)cout<<"273-Free "<<Hiden_1_output<<endl;
  cudaFree(Hiden_2_output);
  if(debug==true)cout<<"275-Free "<<Hiden_2_output<<endl;
  cudaFree(Hiden_3_output);
  if(debug==true)cout<<"277-Free "<<Hiden_3_output<<endl;
  cudaFree(Output_output);
  if(debug==true)cout<<"279-Free "<<Output_output<<endl;
}


void ANN_Struct::cuda_cal(double * device_input_vector){
  //print_device(device_input_vector,1,input_size);
  
  if(debug==true)cout<<"OUTPUT - INPUT_LAYER\n";
  Input_neuron_layer->calculate_layer(device_input_vector);
  Input_output = Input_neuron_layer->get_output_vector();
  if(debug==true)print_device(Input_output,1,hiden_size);
  

  if(debug==true)cout<<"OUTPIT - 1_HIDEN_LAYER\n";
  cudaDeviceSynchronize();
  Hiden_1_layer->calculate_layer(Input_neuron_layer->get_output_vector());
  Hiden_1_output = Hiden_1_layer->get_output_vector();
  if(debug==true)print_device(Hiden_1_output,1,hiden_size);
  //cout<<"1\n";

  if(debug==true)cout<<"OUTPUT - 2_HIDEN_LAYER\n";
  cudaDeviceSynchronize();
  Hiden_2_layer->calculate_layer(Hiden_1_layer->get_output_vector());
  Hiden_2_output = Hiden_2_layer->get_output_vector();
  if(debug==true)print_device(Hiden_2_output,1,hiden_size);
  //cout<<"2\n";


  if(debug==true)cout<<"OUTPUT - 3_HIDEN_LAYER\n";
  cudaDeviceSynchronize();
  Hiden_3_layer->calculate_layer(Hiden_2_layer->get_output_vector());
  Hiden_3_output = Hiden_3_layer->get_output_vector();
  if(debug==true)print_device(Hiden_3_layer->get_output_vector(),1,hiden_size);
  //cout<<"3\n";
  
  
  if(debug==true)cout<<"OUTPUT - OUTPUT_LAYER - Output NET\n";
  cudaDeviceSynchronize();
  Output_neuron_layer->calculate_layer(
  Hiden_3_layer->get_output_vector()
  //Input_neuron_layer->get_output_vector()
  );
  Output_output = Output_neuron_layer->get_output_vector();
  //cout<<"O\n";
}
void ANN_Struct::cudaLearn(double * X_vector, double * correct_output){
  this->cuda_cal(X_vector);
  //roznica correct_output i ^outputu

  //liczenie bledu dla wszystkich warstw
  //poprawianie wag w wszystkich warstwach 
}

//Koniec struktury sieci


//Testy

void test(){
  unsigned int i_size=10;
  unsigned int o_size=10;
  double * X_vector=new double [i_size];
  for(int i=0;i<i_size;i++){
    X_vector[i]=0.2;
  }
  double * device_X_vector_input;
  cudaMalloc((void**)&device_X_vector_input,sizeof(double)*i_size);
  if(debug==true)cout<<"323-Zalokowano: "<<sizeof(double)*i_size<<" pod adresem "<<device_X_vector_input<<" funkcja test"<<endl;
  alloc_size+=sizeof(double)*i_size;

  cudaMemcpy(device_X_vector_input,X_vector,sizeof(double)*i_size,cudaMemcpyHostToDevice);
  if(debug==true)cout<<"368-Kopiowanie pamieci "<<device_X_vector_input<<" "<<X_vector<<" "<<sizeof(double)*i_size<<" H-T-D"<<endl;

  ANN_Struct * Gideon;
  //Neuron_layer ADA(i_size,o_size,0.5);
  //ADA.calculate_layer(device_X_vector_input);
  //print_device(ADA.get_output_vector(),1,o_size);

  //for(int i=50;i<100001;i=i+50){
  
  int zzzz = 10;
  Gideon = new ANN_Struct(i_size,zzzz,o_size,0.5);

  //cout<<"INPUT\n";
  //print_device(device_X_vector_input,1,i_size);
  clock_t start = clock();
  for(int z=0;z<100000;z++){
    Gideon->cal(X_vector);
  }
  cout<<zzzz<<"\t"<<(clock()-start)/(100000.0*CLOCKS_PER_SEC)<<endl;
  //print_device(device_X_vector_input,1,i_size);
  print_device(Gideon->get_device_output_vector(),1,o_size);
  delete Gideon;
  //}
  cout<<"calkowicie zaloowana pamiec to "<<alloc_size<<endl;

}

//Koniec testow


//Funkcja main


int main(){
  //cal();
  //Ler();
  test();
  //for(int i=0;i<10000;i++){
    //cudaDeviceSynchronize();
    //cudaError_t error = cudaGetLastError();
    //if(error!=cudaSuccess)
    //{
    //cout<<cudaGetErrorString(error)<<endl;
    //exit(-1);
    //}
    //else exit(1);;
//}
  return 0;
}
