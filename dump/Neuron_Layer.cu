#ifndef NEURON_LAYER_H
#define NEURON_LAYER_H

#include <cuda.h>
#include <cstdlib>

class Neuron_layer{
  private:
  //public:
  double * device_Scales_mat;

  double * device_output_vector;

  unsigned int input_size;
  unsigned int output_size;
  double learning_ratio;
  public:
  double * get_output_vector(){return device_output_vector;}
  double * get_Scales_mat(){return device_Scales_mat;}
  void calculate_layer(double* X_Vector);
  void Learn(double * X_Vector, double * delta_error);
  //Neuron_layer(){}
  void set_ratio(double ratio){learning_ratio = ratio;}

  Neuron_layer(unsigned int in, unsigned int out,double ratio){
    cudaMalloc((void**)&device_Scales_mat,sizeof(double)*in*out);
    cudaMalloc((void**)&device_output_vector,sizeof(double)*out);
    
    learning_ratio = ratio;
    input_size = in;
    output_size = out;
    
    double rand_num;
    for(unsigned int i=0; i<in*out; i++){
      rand_num = 1;
      //rand()%10000*0.0001;
      //cout<<rand_num<<endl;
      cudaMemcpy(this->device_Scales_mat+i, &rand_num, sizeof(double), cudaMemcpyHostToDevice);
    }
  }
};
__global__ void pre_sum(double * Scales, double* X_Vector, double* out){
  unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
  extern __shared__ double temp[];
  temp[threadIdx.x] = X_Vector[threadIdx.x];
  __syncthreads();
  out[i]=Scales[i]*temp[threadIdx.x];
}

__global__ void sum_row(double* mat,double* out,unsigned int size){
  double sum=0;
  for(unsigned int i=0;i<size;i++){
    sum+=mat[threadIdx.x*size+i];
  }  
  out[threadIdx.x]=sum;
}

__global__ void activation_foo(double * sum_vector, double * output_vector){
  output_vector[threadIdx.x]=__drcp_rn(1+ exp(-sum_vector[threadIdx.x]));
}

__global__ void derivative(double * input, double * output ){  
  output[threadIdx.x]=__dmul_rn(input[threadIdx.x],(1-input[threadIdx.x]));
}

__global__ void mul_matrix(double * mat, double pam,double * ret){
   unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
   ret[i]=(mat[i]*pam);
}

__global__ void mul_matrix_cross(double * A , double * B,double * C){
    unsigned int i= blockDim.x*blockIdx.x + threadIdx.x;
    extern __shared__ double temp [];
    temp[threadIdx.x] = B[threadIdx.x];
    __syncthreads();
    C[i]=__dmul_rn(A[blockIdx.x],temp[threadIdx.x]);
}

__global__ void matrix_update(double * A , double * B, double * C){
  unsigned int i = blockDim.x * blockIdx.x + threadIdx.x;
  C[i]=A[i]+B[i];
}

void Neuron_layer::calculate_layer(double* X_Vector_device){
  
  double * d_p_sum;
  cudaMalloc((void**)&d_p_sum,sizeof(double)*input_size*output_size);

  pre_sum<<<this->output_size, this->input_size, this->input_size>>>(this->device_Scales_mat,X_Vector_device, d_p_sum);
  
  cudaDeviceSynchronize();
  double * d_sum_vector;
  cudaMalloc((void**)&d_sum_vector,sizeof(double)*this->output_size);

  sum_row<<<1, this->output_size>>>(d_p_sum,  d_sum_vector,  this->input_size);

  cudaFree(d_p_sum);
  cudaDeviceSynchronize();

  activation_foo<<<1, this->output_size>>>(d_sum_vector,this->device_output_vector);
  cudaFree(d_sum_vector);
  cudaDeviceSynchronize();
  return;
}
void Neuron_layer::Learn(double * X_Vector, double * delta_error){
  //this->print_scales();
  
  this->calculate_layer(X_Vector);
  
  double * de;
  cudaMalloc((void**)&de,sizeof(double)*output_size);
  derivative<<<1,output_size>>>(this->get_output_vector(),de);
  //print_device(de,output_size,1);
  
  double * iii;
  cudaMalloc((void**)&iii,sizeof(double)*output_size);
  pre_sum<<<1,output_size,output_size>>>(de,delta_error,iii);
  //print_device(iii,output_size,1);

  double * param;
  cudaMalloc((void**)&param,sizeof(double)*output_size);
  mul_matrix<<<1,output_size>>>(iii,learning_ratio,param);
  //print_device(param,output_size,1);

  //print_device(X_Vector,1,input_size);

  double * delta_scales;
  cudaMalloc((void**)&delta_scales,sizeof(double)*input_size*output_size);
  mul_matrix_cross<<<output_size,input_size,input_size>>>(param,X_Vector,delta_scales);
  //print_device(delta_scales,output_size,input_size);

  double * temp;
  cudaMalloc((void**)&temp,sizeof(double)*input_size*output_size);
  matrix_update<<<output_size,input_size>>>(this->device_Scales_mat,delta_scales,temp);
  //print_device(temp,output_size,input_size);
  
  cudaFree(device_Scales_mat);
  device_Scales_mat=temp;

  //this->print_scales();
  return;
}
#endif
