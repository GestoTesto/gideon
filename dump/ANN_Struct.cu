#ifndef ANN_STRUCTUR_H
#define ANN_STRUCTUR_H
#include "Neuron_Layer.cu"
#include <cuda.h>

class ANN_Struct{
void cuda_cal	(double * device_input_vector);
void cal	(double * input_vector);
void cudaLearn	(double * device_input_vector, double * device_output_vector);
void Learn	(double * input_vector, double * output_vector);

double * get_device_output_vector(){return Output_output;}

Neuron_layer Input_neuron_layer;
Neuron_layer Hiden_1_layer;
Neuron_layer Hiden_2_layer;
Neuron_layer Hiden_3_layer;
Neuron_layer Output_neuron_layer;

double * Input_output;
double * Hiden_1_output;
double * Hiden_2_output;
double * Hiden_3_output;
double * Output_output;

unsigned int input_size;
unsigned int hiden_size;
unsigned int output_size;

};
#endif
