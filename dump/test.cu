#ifndef TEST_H
#define TEST_H

#include <iostream>
#include "cuda.h"
#include "Neuron_Layer.cu"

using namespace std; 

void test_cal();

void print_device(double * mat, unsigned int h, unsigned int w){
double * temp = new double [h*w];
cudaMemcpy(temp,mat,sizeof(double)*h*w,cudaMemcpyDeviceToHost);
for(int i=0;i<h;i++){
  for(int j=0;j<w;j++){
    cout<<temp[i*w+j]<<" ";  
  }  
  cout<<endl;
}
cout<<endl;

}

void test(){
  //test_layer();  
  test_cal();
}
void test_layer(){
  try{
    Neuron_layer n(7,9,0.5);
    double * X = new double [7];
    double * D = new double [9];
    double * E = new double [9];

    for(int i=0;i<9;i++){
      D[i]=0.5 ; 
    }
    for(int i=0;i<7;i++){
      X[i]=0;  
    }

    double * d_x, *d_d, *d_e;
    cudaMalloc((void**)&d_x, sizeof(double)*7);
    cudaMalloc((void**)&d_d, sizeof(double)*9);
    cudaMalloc((void**)&d_e, sizeof(double)*9);

    cudaMemcpy(d_x,X,sizeof(double)*7, cudaMemcpyHostToDevice);
    cudaMemcpy(d_d,D,sizeof(double)*9, cudaMemcpyHostToDevice);
    
    print_device(n.get_Scales_mat(),9,7);
    n.Learn(d_x,d_d);
    cout<<endl;
    print_device(n.get_Scales_mat(),9,7);
    
    //cudaMemcpy(d_e,E,sizeof(double)*9, cudaMemcpyHostToDevice);

  }
  catch(...){
    cout<<"ERROR: Test layer";  
  }
}
void pre_sum_test(){
    double * X = new double [7];
    double * D = new double [9];
    double * E = new double [9];

    for(int i=0;i<9;i++){
      D[i]=0.5 ; 
    }
    for(int i=0;i<7;i++){
      X[i]=0;  
    }

    double * d_x, *d_d, *d_e;
    cudaMalloc((void**)&d_x, sizeof(double)*7);
    cudaMalloc((void**)&d_d, sizeof(double)*9);
    cudaMalloc((void**)&d_e, sizeof(double)*9);

    cudaMemcpy(d_x,X,sizeof(double)*7, cudaMemcpyHostToDevice);
    cudaMemcpy(d_d,D,sizeof(double)*9, cudaMemcpyHostToDevice);

}


void test_cal(){
  try{
    Neuron_layer n(7,9,0.5);
    double * X = new double [7];
    double * D = new double [9];
    double * E = new double [9];

    for(int i=0;i<9;i++){
      D[i]=0.5 ; 
    }
    for(int i=0;i<7;i++){
      X[i]=1;  
    }

    double * d_x, *d_d, *d_e;
    cudaMalloc((void**)&d_x, sizeof(double)*7);
    cudaMalloc((void**)&d_d, sizeof(double)*9);
    cudaMalloc((void**)&d_e, sizeof(double)*9);

    cudaMemcpy(d_x,X,sizeof(double)*7, cudaMemcpyHostToDevice);
    cudaMemcpy(d_d,D,sizeof(double)*9, cudaMemcpyHostToDevice);
    
    n.calculate_layer(d_x);
    double * d_oo =n.get_output_vector();

    cudaMemcpy(E,d_oo,sizeof(double)*9, cudaMemcpyHostToDevice);
  
    for(int i=0;i<9;i++){
      cout<<E[i]<<endl;  
    }
  }
  catch(...){
    cout<<"ERROR: Test layer";  
  }    
}


void cal(){
  int in_size=3;
  int out_size=4;
  
  double * in_vector = new double [in_size];
  double * out_vector = new double [out_size];
  double * delta = new double [out_size];
  for(int i=0;i<in_size;i++){
    in_vector[i]=1;  
  }
  for(int i=0;i<out_size;i++){
    delta[i]=0.5;  
  }
  double *d_x,*d_o;
  cudaMalloc((void**)&d_x,sizeof(double)*in_size);
  cudaMalloc((void**)&d_o,sizeof(double)*out_size);
  
  cudaMemcpy(d_x,in_vector,sizeof(double)*in_size,cudaMemcpyHostToDevice);
  cudaMemcpy(d_o,delta,sizeof(double)*out_size,cudaMemcpyHostToDevice);


  Neuron_layer n(3,4,0.5);
  n.calculate_layer(d_x);
  cout<<endl; 

  print_device(d_o,out_size,1);
}

void Ler(){
  int in_size=3;
  int out_size=4;
  
  double * in_vector = new double [in_size];
  double * out_vector = new double [out_size];
  double * delta = new double [out_size];
  for(int i=0;i<in_size;i++){
    in_vector[i]=1;  
  }
  for(int i=0;i<out_size;i++){
    delta[i]=0.5;  
  }
  double *d_x,*d_o;
  cudaMalloc((void**)&d_x,sizeof(double)*in_size);
  cudaMalloc((void**)&d_o,sizeof(double)*out_size);
  
  cudaMemcpy(d_x,in_vector,sizeof(double)*in_size,cudaMemcpyHostToDevice);
  cudaMemcpy(d_o,delta,sizeof(double)*out_size,cudaMemcpyHostToDevice);


  Neuron_layer n(3,4,0.5);
  n.Learn(d_x,d_o);
  cout<<endl;
  n.calculate_layer(d_x);


 cudaMemcpy(out_vector,n.get_output_vector(),sizeof(double)*out_size,cudaMemcpyDeviceToHost);
  for(int i=0;i<out_size;i++){
    cout<<out_vector[i]<<endl;  
  } 
}
#endif

